provider "aws" {
  region = var.region
}

data "aws_caller_identity" "current" {}

module "ecr-pipeline" {
  region    = var.region
  repo-name = "apache"
  source    = "git::https://gitlab.com/colmmg/terraform/fargate-ssh/modules/ecr-pipeline.git?ref=master"
}
